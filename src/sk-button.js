
import { SkElement } from '../../sk-core/src/sk-element.js';

import { BTN_FORWARDED_ATTRS } from './impl/sk-button-impl.js';

export class SkButton extends SkElement {

    get buttonEl() {
        return this.el ? this.el.querySelector('button') : null;
    }

    get cnSuffix() {
        return 'button';
    }

    get impl() {
        if (!this._impl) {
            this.initImpl();
        }
        return this._impl;
    }
    attributeChangedCallback(name, oldValue, newValue) {
        if (name === 'disabled' && oldValue !== newValue && this.implRenderTimest) {
            if (newValue) {
                this.impl.disable();
            } else {
                this.impl.enable();
            }
        }
        if (BTN_FORWARDED_ATTRS[name] && oldValue !== newValue) {
            if (newValue !== null && newValue !== undefined) {
                if (this.buttonEl) {
                    this.buttonEl.setAttribute(name, newValue);
                }
            } else {
                this.buttonEl.removeAttribute(name);
            }
        }
        this.callPluginHook('onAttrChange', name, oldValue, newValue);
    }

    static get observedAttributes() {
        return Object.keys(BTN_FORWARDED_ATTRS);
    }

}
