

import { SkComponentImpl } from '../../../sk-core/src/impl/sk-component-impl.js';

export var BTN_FORWARDED_ATTRS = { 'type': 'type', 'disabled': 'disabled' };

export class SkButtonImpl extends SkComponentImpl {

    get suffix() {
        return 'button';
    }

    get buttonEl() {
        if (! this._buttonEl) {
            this._buttonEl = this.comp.el.querySelector('button');
        }
        return this._buttonEl;
    }

    set buttonEl(el) {
        this._buttonEl = el;
    }

    get button() {
        if (! this._button) {
            this._button = this.comp.el.querySelector('button');
        }
        return this._button;
    }

    set button(el) {
        this._button = el;
    }

    get subEls() {
        return [ 'buttonEl', 'button'];
    }

    get buttonType() {
        return this.comp.getAttribute('button-type');
    }

    enable() {
        super.enable();
        this.buttonEl.removeAttribute('disabled');
    }

    disable() {
        super.disable();
        //this.buttonEl.setAttribute('disabled', 'disabled');
    }

    restoreState(state) {
        super.restoreState(state);
        this.forwardAttributes();
    }

    forwardAttributes() {
        if (this.buttonEl) {
            for (let attrName of Object.keys(BTN_FORWARDED_ATTRS)) {
                let value = this.comp.getAttribute(attrName);
                if (value) {
                    this.buttonEl.setAttribute(attrName, value);
                } else {
                    if (this.comp.hasAttribute(attrName)) {
                        this.buttonEl.setAttribute(attrName, '');
                    }
                }
                if (attrName === 'list' && value && this.comp.useShadowRoot) { // import datalist into shadow root
                    this.comp.el.append(document.importNode(document.getElementById(value), true));
                }
            }
        }
    }

    bindEvents() {
        super.bindEvents();
        if (this.comp.getAttribute('type') === 'submit') {
            this.buttonEl.onclick = function(event) {
                this.comp.callPluginHook('onEventStart', event);
                this.onSubmit(event);
                this.comp.callPluginHook('onEventEnd', event);
            }.bind(this);
        } else {
/*
            this.buttonEl.onclick = function(event) {
                this.onClick(event);
            }.bind(this);
*/
        }
    }
/*

    onClick(event) {
        this.comp.el.dispatchEvent(new CustomEvent('skclick', { target: this.comp, bubbles: true, composed: true }));
        this.comp.el.dispatchEvent(new CustomEvent('click', { target: this.comp, bubbles: true, composed: true }));
    }
*/

    onSubmit(event) {
        this.comp.el.dispatchEvent(new CustomEvent('formsubmit', { target: this.comp, bubbles: true, composed: true }));
    }

    unbindEvents() {
        super.unbindEvents();
    }

    afterRendered() {
        super.afterRendered();
        this.restoreState();
    }

    focus() {
        setTimeout(() => {
            this.comp.buttonEl.focus();
        }, 0);
    }
}